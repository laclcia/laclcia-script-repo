# laclcia script repo

a repo for all my loose script's. hope it's usefull to someone
## Installer
the auto installer/updater simply run the following

<br>

```
cd ~
mkdir ~/ubin
wget https://gitlab.com/laclcia/laclcia-script-repo/-/raw/master/update-scripts -P ~/ubin/
cd ubin
chmod +x update-scripts
./update-scripts
```

<br>

Never run random script's you find on the internet without checking what they do. 

<br>

## Usage

```
update # to update the base system
    update -f # to update 
    upcate -c # clear the pacman cache
    update -h # show help menu

update-aur # to update aur packadges
    update-aur -n # to update nvidia drivers
    update-aur -m # to compile/update mesa-git and lib32-mesa-git
    upcate-aur -c # clear the yay cache
    update-aur -h # to show the help menu

update-hosts # updates hosts file to do adblocking plus install satisfactory/epic redirection to make those game work

update-scripts # to update all scripts to lastest version

gamermode # to switch cpu/kernel policy to maximise gaming performences. may overheat laptops (this get's reverted on reboot)

normalmode # revert's to normal behaviour from gamermode without a reboot.

```

<br>

feel free to check the code of every script's here. and havee a nice day

<br>

^_^

<br>
last updated jully 30th 2023
